//
//  MultiplicationTableLearnerApp.swift
//  MultiplicationTableLearner
//
//  Created by Pascal Hintze on 02.11.2023.
//

import SwiftUI

@main
struct MultiplicationTableLearnerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  ContentView.swift
//  MultiplicationTableLearner
//
//  Created by Pascal Hintze on 02.11.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var startingNumber = 1
    @State private var endingNumber = 12
    
    @State private var numberOfQuestions = 5
    @State private var currentQuestion: Int = 0
    @State private var questions: [Questions] = []
    
    @State private var gameStarted = false
    
    func start() -> any View {
        ContentView()
    }
    
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                
                Stepper("Startpoint: \(startingNumber)", value: $startingNumber, in: 1...12)
                    .padding()
                Stepper("Endpoint: \(endingNumber)", value: $endingNumber, in: 1...12)
                    .padding()
                Spacer()
                
                Text("Multiplication Table \(startingNumber) x \(endingNumber)")
                
                Spacer()
                
                Section("Choose number of questions: \(numberOfQuestions)") {
                    HStack {
                        Button("5", action: {numberOfQuestions = 5})
                        Spacer()
                        Button("10", action: {numberOfQuestions = 10})
                        Spacer()
                        Button("20", action: {numberOfQuestions = 20})
                    }
                    .padding(60)
                }
                
                Spacer()
                
                Button("Generate questions", action: {
                    for _ in 0..<numberOfQuestions {
                        questions.append(Questions(x: Int.random(in: startingNumber...endingNumber), y: Int.random(in: startingNumber...endingNumber)))
                        print(questions)
                        
                    }
                    
                })
                
                Spacer()
                
                NavigationLink("Show Questions") {
                    QuestionsView(questions: questions)
                }
                Spacer()
            }
        }
    }
}

#Preview {
    ContentView()
}

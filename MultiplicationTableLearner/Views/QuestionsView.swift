//
//  QuestionsView.swift
//  MultiplicationTableLearner
//
//  Created by Pascal Hintze on 03.11.2023.
//

import SwiftUI

struct QuestionsView: View {
    var questions: [Questions]
    @State private var answer: String = ""
    @State private var questionCounter = 0
    @State private var gameEnded = false
    
    @State private var showPopover = false
    @State private var correctAnswers = 0
    
    
    var body: some View {
        NavigationStack {
            HStack {
                VStack {
                    Spacer()
                    Text(questions[questionCounter].questionText)
                    Spacer()
                    TextField("Answer:", text: $answer) {
                        if Int(answer) == questions[questionCounter].result {
                            correctAnswers += 1
                        }
                        showPopover = true
                    }
                    
                    .alert("Result", isPresented: $showPopover) {
                        Button("Next", action: nextQuestion)
                    } message: {
                        if Int(answer) == questions[questionCounter].result {
                            Text("\(answer) is correct!")
                        } else {
                            Text("Wrong answer. The correct answer is \(questions[questionCounter].result)")
                        }
                    }
                    .textFieldStyle(.roundedBorder)
                    Spacer()
                    Button("Skip", action: nextQuestion)
                    Spacer()
                    
                }
            }
            .alert("End Result", isPresented: $gameEnded) {
                NavigationLink("Restart") {
                    ContentView()
                }
            } message: {
                Text("You answered ^[\(correctAnswers) question](inflect: true) out of ^[\(questions.count) question](inflect: true) correctly.")
            }
        }
    }
    
    func nextQuestion() {
        if questionCounter < (questions.count - 1) {
            questionCounter += 1
            print(questionCounter)
        } else {
            gameEnded = true
        }
        answer = ""
    }
    
    func restartGame() {
        
    }
}

#Preview {
    QuestionsView(questions: [Questions(x: 1, y: 1), Questions(x: 2, y: 2)])
}

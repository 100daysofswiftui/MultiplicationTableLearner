//
//  Questions.swift
//  MultiplicationTableLearner
//
//  Created by Pascal Hintze on 03.11.2023.
//

import Foundation

struct Questions: Identifiable {
    var questionText: String {
        "What is \(x) x \(y) ?"
    }
    let x: Int
    let y: Int
    var result: Int {
         x * y
    }
    var id = UUID()
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
}

# Multiplication Table Learner

Multiplication Table Learner is an app to help the user to practice multiplication tables.

This app was developed as a challenge from the [100 Days of SwiftUI](https://www.hackingwithswift.com/guide/ios-swiftui/3/3/challenge) course from Hacking with Swift.